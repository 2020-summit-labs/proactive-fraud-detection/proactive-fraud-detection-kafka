package com.redhat.summit.model;

import io.quarkus.runtime.annotations.RegisterForReflection;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@NoArgsConstructor
@RegisterForReflection
public class CreditCardTransaction implements Comparable<CreditCardTransaction> {

    private String transactionReference;
    private Double amount;
    private LocalDateTime date;

    // Account Meta
    private String cardHolderName;
    private String cardNumber;
    private String cardType;
    private String cardBrand;

    // TX Meta Data
    private String merchantId;
    private String authCode;
    private String posId;
    private String location;

    @Override
    public int compareTo(CreditCardTransaction o) {
        return this.getDate().compareTo(o.getDate());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + ((authCode == null) ? 0 : authCode.hashCode());
        result = prime * result + ((cardBrand == null) ? 0 : cardBrand.hashCode());
        result = prime * result + ((cardHolderName == null) ? 0 : cardHolderName.hashCode());
        result = prime * result + ((cardNumber == null) ? 0 : cardNumber.hashCode());
        result = prime * result + ((cardType == null) ? 0 : cardType.hashCode());
        result = prime * result + ((date == null) ? 0 : date.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((merchantId == null) ? 0 : merchantId.hashCode());
        result = prime * result + ((posId == null) ? 0 : posId.hashCode());
        result = prime * result + ((transactionReference == null) ? 0 : transactionReference.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CreditCardTransaction other = (CreditCardTransaction) obj;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (!amount.equals(other.amount))
            return false;
        if (authCode == null) {
            if (other.authCode != null)
                return false;
        } else if (!authCode.equals(other.authCode))
            return false;
        if (cardBrand == null) {
            if (other.cardBrand != null)
                return false;
        } else if (!cardBrand.equals(other.cardBrand))
            return false;
        if (cardHolderName == null) {
            if (other.cardHolderName != null)
                return false;
        } else if (!cardHolderName.equals(other.cardHolderName))
            return false;
        if (cardNumber == null) {
            if (other.cardNumber != null)
                return false;
        } else if (!cardNumber.equals(other.cardNumber))
            return false;
        if (cardType == null) {
            if (other.cardType != null)
                return false;
        } else if (!cardType.equals(other.cardType))
            return false;
        if (date == null) {
            if (other.date != null)
                return false;
        } else if (!date.equals(other.date))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (merchantId == null) {
            if (other.merchantId != null)
                return false;
        } else if (!merchantId.equals(other.merchantId))
            return false;
        if (posId == null) {
            if (other.posId != null)
                return false;
        } else if (!posId.equals(other.posId))
            return false;
        if (transactionReference == null) {
            if (other.transactionReference != null)
                return false;
        } else if (!transactionReference.equals(other.transactionReference))
            return false;
        return true;
    }
}