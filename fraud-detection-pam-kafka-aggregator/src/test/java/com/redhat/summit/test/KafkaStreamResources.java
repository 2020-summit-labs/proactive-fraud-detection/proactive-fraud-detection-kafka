package com.redhat.summit.test;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;

import io.debezium.kafka.KafkaCluster;
import io.debezium.util.Testing;
import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

@ApplicationScoped
public class KafkaStreamResources implements QuarkusTestResourceLifecycleManager {

    private KafkaCluster cluster;

    private File dataDir;

    @Override
    public Map<String, String> start() {
        try {
            Properties props = new Properties();
            props.setProperty("zookeeper.connection.timeout.ms", "45000");
            dataDir = Testing.Files.createTestingDirectory("kafka-data", true);
            cluster = new KafkaCluster().withPorts(2182, 9092).addBrokers(1).usingDirectory(dataDir)
                    .deleteDataUponShutdown(true).withKafkaConfiguration(props).deleteDataPriorToStartup(true)
                    .startup();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return Collections.emptyMap();
    }

    @Override
    public void stop() {
        cluster.shutdown();
        Testing.Files.delete(dataDir);
    }
}