package com.redhat.summit.test;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import com.redhat.summit.model.CreditCardTransaction;
import com.redhat.summit.test.util.KafkaTestUtil;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.junit.After;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
@QuarkusTestResource(KafkaStreamResources.class)
public class KafkaStreamTransactionsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaStreamTransactionsTest.class);

    private final String clientId = UUID.randomUUID().toString();

    @Inject
    private KafkaTestUtil testUtil;

    @Inject
    private KafkaStreams streams;

    private Producer<String, CreditCardTransaction> producer;

    private Client client;
    
    private WebTarget target;

    @Test
    public void testPricesEventStream() {
        client = ClientBuilder.newClient();
        target = client.target("http://localhost:8081").path("transactions").path("data").path("1234567891011121");
        // init the producer
        producer = new KafkaProducer<String, CreditCardTransaction>(testUtil.baseProducerProps(clientId));
        // clean existing data from the ktable
        streams.cleanUp();
        // generate 10 random transactions and read aggregated transactions
        final List<CreditCardTransaction> firstResults = generateWaitAndGetAggregatedTransactions(10);
        // generate 10 random transactions and read aggregated transactions
        final List<CreditCardTransaction> secondResults = generateWaitAndGetAggregatedTransactions(10);
        // Assertions
        for(final CreditCardTransaction result : secondResults){
            assertTrue(!firstResults.contains(result));
        }
        //graceful shutdown
        client.close();
        producer.close();
        streams.close();
    }

    public List<CreditCardTransaction> generateWaitAndGetAggregatedTransactions(final int numberOfMessages){
        IntStream.range(0, numberOfMessages)
        .mapToObj(i -> testUtil.buildCCTX(testUtil.getTestAccount(), (double) ThreadLocalRandom.current().nextInt(1, 10))) //
        .map(transaction -> {
            LOGGER.info("publishing {}", transaction);
            return transaction;
        }) //
        .forEach(transaction -> {
            try {
                producer.send(new ProducerRecord<>("transactions-topic", transaction.getCardNumber(), transaction)).get();
            } catch(Exception e) {
                e.printStackTrace();
            }
        });
        // wait the stream to be up and running
        await().atMost(100000, MILLISECONDS).until(() -> streams.state().isRunning());
        // wait for the aggregator
        try {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // read aggregated transactions
        final Response response = target.request().get();
        final List<CreditCardTransaction> results = response.readEntity(new GenericType<List<CreditCardTransaction>>() {});
        response.close();
        return results;
    }
}